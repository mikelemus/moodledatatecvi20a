<?php

class __Mustache_4286113d38d68bfa81e962f745469230 extends Mustache_Template
{
    private $lambdaHelper;

    public function renderInternal(Mustache_Context $context, $indent = '')
    {
        $this->lambdaHelper = new Mustache_LambdaHelper($this->mustache, $context);
        $buffer = '';

        $buffer .= $indent . '
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '<style>
';
        $buffer .= $indent . 'body:not(.dir-ltr):not(.dir-rtl) .btn-insight {
';
        $buffer .= $indent . '    margin-bottom: 1rem!important;
';
        $buffer .= $indent . '    margin-right: 1rem!important;
';
        $buffer .= $indent . '    color: #212529;
';
        $buffer .= $indent . '    background-color: #e9ecef;
';
        $buffer .= $indent . '    border-color: #e9ecef
';
        $buffer .= $indent . '    display: inline-block;
';
        $buffer .= $indent . '    font-weight: 400;
';
        $buffer .= $indent . '    text-align: center;
';
        $buffer .= $indent . '    white-space: nowrap;
';
        $buffer .= $indent . '    vertical-align: middle;
';
        $buffer .= $indent . '    user-select: none;
';
        $buffer .= $indent . '    border: 1px solid transparent;
';
        $buffer .= $indent . '    padding: .375rem .75rem;
';
        $buffer .= $indent . '    font-size: .9375rem;
';
        $buffer .= $indent . '    line-height: 1.5;
';
        $buffer .= $indent . '    border-radius: .25rem;
';
        $buffer .= $indent . '    text-decoration: none;
';
        $buffer .= $indent . '}
';
        $buffer .= $indent . '</style>
';
        $buffer .= $indent . '
';
        $buffer .= $indent . '<br/>
';
        // 'actions' section
        $value = $context->find('actions');
        $buffer .= $this->section87513cfdb56be22e65c7e0cce4d364f1($context, $indent, $value);

        return $buffer;
    }

    private function section87513cfdb56be22e65c7e0cce4d364f1(Mustache_Context $context, $indent, $value)
    {
        $buffer = '';
    
        if (!is_string($value) && is_callable($value)) {
            $source = '
    <a class="btn btn-default m-r-1 m-b-1 btn-insight" href="{{url}}">{{text}}</a>
';
            $result = call_user_func($value, $source, $this->lambdaHelper);
            if (strpos($result, '{{') === false) {
                $buffer .= $result;
            } else {
                $buffer .= $this->mustache
                    ->loadLambda((string) $result)
                    ->renderInternal($context);
            }
        } elseif (!empty($value)) {
            $values = $this->isIterable($value) ? $value : array($value);
            foreach ($values as $value) {
                $context->push($value);
                
                $buffer .= $indent . '    <a class="btn btn-default m-r-1 m-b-1 btn-insight" href="';
                $value = $this->resolveValue($context->find('url'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '">';
                $value = $this->resolveValue($context->find('text'), $context);
                $buffer .= call_user_func($this->mustache->getEscape(), $value);
                $buffer .= '</a>
';
                $context->pop();
            }
        }
    
        return $buffer;
    }

}
